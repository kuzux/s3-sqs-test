import boto3
import requests

import sys
import pathlib

session = boto3.Session(profile_name="personal-take2", region_name="eu-west-1")
client = session.client('s3')
path = pathlib.Path(sys.argv[1])
response = client.generate_presigned_post('sqs-notify-test-bucket', path.name)

with path.open("rb") as f:
    files = {'file': (path.name, f)}
    http_response = requests.post(response['url'], data=response['fields'], files=files)

print("Done.")