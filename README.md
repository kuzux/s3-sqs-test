This was a simple experiment for uploading a file to S3 (using a presigned URL) and getting notifications from AWS for that upload. Now it is a simple code example :)

# Findings

* We can create notifications for S3 uploads on SQS using Terraform
* We can listen to / consume SQS messages without any webhooks (But with a couple of seconds of delay)
* Creating a presigned URL on the S3 bucket does not trigger a notification, but uploading to it does
* Since we specify the key when generating a presigned URL, we can use the key attribute of the SQS notification to identify the upload
