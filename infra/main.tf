terraform {
    required_providers {
        aws = {
            source  = "hashicorp/aws"
            version = "~> 4.16"
        }
    }

    required_version = ">= 1.2.0"
}

provider "aws" {
    region = "eu-west-1"
    shared_credentials_files = ["~/.aws/credentials"]
    profile = "personal-take2"
}

data "aws_iam_policy_document" "queue" {
    statement {
        effect = "Allow"

        principals {
            type        = "*"
            identifiers = ["*"]
        }

        actions   = ["sqs:SendMessage"]
        resources = ["arn:aws:sqs:*:*:s3-event-notification-queue"]

        condition {
            test     = "ArnEquals"
            variable = "aws:SourceArn"
            values   = [aws_s3_bucket.bucket.arn]
        }
    }
}

resource "aws_sqs_queue" "queue" {
    name   = "s3-event-notification-queue"
    policy = data.aws_iam_policy_document.queue.json
}

resource "random_id" "example" {
  byte_length = 8
}

resource "aws_s3_bucket" "bucket" {
    bucket = "sqs-notify-test-bucket-${random_id.example.hex}"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
    bucket = aws_s3_bucket.bucket.id

    queue {
        queue_arn     = aws_sqs_queue.queue.arn
        events        = ["s3:ObjectCreated:*"]
    }
}
