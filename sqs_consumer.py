import boto3
import sys
import json

session = boto3.Session(profile_name="personal-take2", region_name="eu-west-1")
sqs = session.resource('sqs')

queue = None
try:
    queue = sqs.get_queue_by_name(QueueName="s3-event-notification-queue")
except:
    print("Could not find queue")
    print(sys.exc_info()[1])
    sys.exit(1)

print('Listening...')
try:
    while True:
        messages = queue.receive_messages(WaitTimeSeconds=5)
        if len(messages) == 0:
            continue
        for message in messages:
            json_body = json.loads(message.body)

            # We get a test message in the beginning
            # TODO: I'm not 100% sure about what to do when there are more than 1 records
            if "Records" not in json_body:
                print("Invalid Message:")
                print(json.dumps(json_body, indent=4))
                message.delete()
                continue

            file = json_body["Records"][0]
            key = file["s3"]["object"]["key"]
            print(f"Got file upload {key}")
            message.delete()
except KeyboardInterrupt:
    print("\nBye.")
    sys.exit(0)
